FROM python:3.10-alpine

WORKDIR /app

COPY . .

RUN pip install -r /app/source_code/requirements.txt

ENTRYPOINT ["python"]

CMD ["source_code/server.py"]
